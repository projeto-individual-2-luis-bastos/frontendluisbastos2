import { clientHttp } from '../config/config.js'

const createCartao = (data) => clientHttp.post(`/cartao`, data)

const updateCartao = (data) => clientHttp.patch(`/cartao/${data._id}`, data)
// TODO: Verificar no back  a atualização

const ListCartao = () => clientHttp.get(`/cartao`)

const DeleteCartao = (id) => clientHttp.delete(`/cartao/${id}`)

const showCartaoId = (id) => clientHttp.patch(`/cartao/${id}`)

export {
    createCartao,
    ListCartao,
    DeleteCartao,
    showCartaoId,
    updateCartao
}