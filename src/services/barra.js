import { clientHttp } from '../config/config.js'

const createBarra = (data) => clientHttp.post(`/barra`, data)

const updateBarra = (data) => clientHttp.patch(`/barra/${data._id}`, data)
// TODO: Verificar no back  a atualização

const ListBarra = () => clientHttp.get(`/barra`)

const DeleteBarra = (id) => clientHttp.delete(`/barra/${id}`)

const showBarraId = (id) => clientHttp.patch(`/barra/${id}`)

export {
    createBarra,
    ListBarra,
    DeleteBarra,
    showBarraId,
    updateBarra
}