import { clientHttp } from '../config/config.js'

const createLeblon = (data) => clientHttp.post(`/leblon`, data)

const updateLeblon = (data) => clientHttp.patch(`/leblon/${data._id}`, data)
// TODO: Verificar no back  a atualização

const ListLeblon = () => clientHttp.get(`/leblon`)

const DeleteLeblon = (id) => clientHttp.delete(`/leblon/${id}`)

const showLeblonId = (id) => clientHttp.patch(`/leblon/${id}`)

export {
    createLeblon,
    ListLeblon,
    DeleteLeblon,
    showLeblonId,
    updateLeblon
}