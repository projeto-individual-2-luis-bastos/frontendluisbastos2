import { clientHttp } from '../config/config.js'

const createGeral = (data) => clientHttp.post(`/geral`, data)

const updateGeral = (data) => clientHttp.patch(`/geral/${data._id}`, data)
// TODO: Verificar no back  a atualização

const ListGeral = () => clientHttp.get(`/geral`)

const DeleteGeral = (id) => clientHttp.delete(`/geral/${id}`)

const showGeralId = (id) => clientHttp.patch(`/geral/${id}`)

export {
    createGeral,
    ListGeral,
    DeleteGeral,
    showGeralId,
    updateGeral
}