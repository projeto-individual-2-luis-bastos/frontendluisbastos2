import { clientHttp } from '../config/config.js'

const createUser = (data) => clientHttp.post(`/envios`, data)

const updateUser = (data) => clientHttp.patch(`/envios/${data._id}`, data)
// TODO: Verificar no back  a atualização

const ListUser = () => clientHttp.get(`/envios`)

const DeleteUser = (id) => clientHttp.delete(`/envios/${id}`)

const showUserId = (id) => clientHttp.patch(`/envios/${id}`)

export {
    createUser,
    ListUser,
    DeleteUser,
    showUserId,
    updateUser
}