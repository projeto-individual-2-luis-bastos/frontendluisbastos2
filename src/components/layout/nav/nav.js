import React from "react";
import "./nav.css";
import history from "../../../config/history";
import { Button } from "reactstrap";
import styled from "styled-components";

const Nav = ({ name, to }) => {
  const changePage = () => history.push(to);

  return (
    <nav className="d-flex">
      <div className="action">
        <ButtonNav onClick={changePage}>{name}</ButtonNav>
      </div>
    </nav>
  );
};
export default Nav;

const ButtonNav = styled(Button)`
border: none;
color: white;
padding: 15px 32px;
text-align: center;
color: red;
text-decoration: none;
display: inline-block;
font-size: 16px;
background-color: pink;
`;

export { ButtonNav };
