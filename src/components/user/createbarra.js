import React, { useState, useEffect } from "react";
import { createBarra, showBarraId, updateBarra } from "../../services/barra";
// import Loading from '../loading/loading'
import Alert from "../alert/index.js";
import Nav from "../layout/nav/nav";
import { useHistory, useParams } from "react-router-dom";
import jwt from "jsonwebtoken";
import { getToken } from "../../config/auth";
import { Button, FormGroup, Label, Input, CustomInput } from "reactstrap";
import "./user.css";

const BarraCreate = (props) => {
  const [userIsAdmin, setUserIsAdmin] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);
  const [isEdit, setisEdit] = useState(false);
  const [alert, setAlert] = useState({});
  const history = useHistory();
  const { id } = useParams();
  const methodUser = isEdit ? updateBarra : createBarra;

  const [form, setForm] = useState({
    is_admin: false,
  });

  useEffect(() => {
    (async () => {
      const { user } = await jwt.decode(getToken());
      setUserIsAdmin(user.is_admin);
    })();
    return () => {};
  }, []);

  useEffect(() => {
    const getShowUser = async () => {
      const user = await showBarraId(id);
      if (user.data.senha) {
        delete user.data.senha;
      }
      setForm(user.data);
    };

    if (id) {
      setisEdit(true);
      getShowUser();
    }
  }, [id]);

  const handleChange = (event) => {
    // const checked = event.target.value === 'on' ? true : false

    const value =
      event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value;
    const name = event.target.name;

    setForm({
      ...form,
      [name]: value,
    });
  };

  const formIsValid = () => {
    return form.mes && form.condominio && form.net && form.luz && form.gas && form.total ;
  };

  const submitForm = async (event) => {
    try {
      setIsSubmit(true);
      await methodUser(form);
      const is_admin = userIsAdmin ? form.is_admin : false;
      setForm({
        ...form,
        is_admin,
      });

      setAlert({
        type: "success",
        message: "Seu formulário foi enviado com sucesso",
        show: true,
      });
      setIsSubmit(false);

      setTimeout(() => history.push("/"), 3000);
    } catch (e) {
      setAlert({
        type: "error",
        message: "Ocorreu um erro no cadastro",
        show: true,
      });
      setIsSubmit(false);
    }
  };

  return (
    <React.Fragment>
      <Nav name="Lista" to="/" />
      <section>
        <Alert
          type={alert.type || ""}
          message={alert.message || ""}
          show={alert.show || false}
        />

        <div className="create_user">
          <div className="form_login">

            <FormGroup>
              <Label for="auth_mes">mes</Label>
              <Input
                disabled={isSubmit}
                type="text"
                id="auth_mes"
                name="mes"
                onChange={handleChange}
                value={form.mes || ""}
                placeholder="Qual é o mês?"
              />
            </FormGroup>

            <FormGroup>
              <Label for="auth_condominio">condominio</Label>
              <Input
                disabled={isSubmit}
                type="text"
                id="auth_condominio"
                name="condominio"
                onChange={handleChange}
                value={form.condominio || ""}
                placeholder="Qual é o valor do condominio?"
              />
            </FormGroup>

            <FormGroup>
              <Label for="auth_net">net</Label>
              <Input
                disabled={isSubmit}
                type="text"
                id="auth_net"
                name="net"
                onChange={handleChange}
                value={form.net || ""}
                placeholder="Qual é o valor da net?"
              />
            </FormGroup>
            <FormGroup>
              <Label for="auth_luz">luz</Label>
              <Input
                disabled={isSubmit}
                type="text"
                id="auth_luz"
                name="luz"
                onChange={handleChange}
                value={form.luz || ""}
                placeholder="Qual é o valor da luz?"
              />
            </FormGroup>
            <FormGroup>
              <Label for="auth_gas">gas</Label>
              <Input
                disabled={isSubmit}
                type= "text"
                id="auth_gas"
                name="gas"
                onChange={handleChange}
                value = {form.gas}
                placeholder="Qual é o valor do gas?"
              />
              
            </FormGroup>
            <FormGroup>
              <Label for="auth_total">total</Label>
              <Input
                disabled={isSubmit}
                type= "text"
                id="auth_total"
                name="total"
                onChange={handleChange}
                value = {form.total}
                placeholder="Qual é o valor total?"
              />
              
            </FormGroup>


            <Button
              color="primary"
              disabled={!formIsValid()}
              onClick={submitForm}
            >
              {isEdit ? "Atualizar" : "Cadastrar"}
            </Button>
          </div>
          <br />
          {/* <Loading show={isSubmit}/> */}
          {isSubmit ? <div>Carregando....</div> : ""}
        </div>

      </section>
    </React.Fragment>
  );
};

export default BarraCreate;