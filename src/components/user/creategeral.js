import React, { useState, useEffect } from "react";
import { createGeral, showGeralId, updateGeral } from "../../services/geral";
// import Loading from '../loading/loading'
import Alert from "../alert/index.js";
import Nav from "../layout/nav/nav";
import { useHistory, useParams } from "react-router-dom";
import jwt from "jsonwebtoken";
import { getToken } from "../../config/auth";
import { Button, FormGroup, Label, Input, CustomInput } from "reactstrap";
import "./user.css";

const GeralCreate = (props) => {
  const [userIsAdmin, setUserIsAdmin] = useState({});
  const [isSubmit, setIsSubmit] = useState(false);
  const [isEdit, setisEdit] = useState(false);
  const [alert, setAlert] = useState({});
  const history = useHistory();
  const { id } = useParams();
  const methodUser = isEdit ? updateGeral : createGeral;

  const [form, setForm] = useState({
    is_admin: false,
  });

  useEffect(() => {
    (async () => {
      const { user } = await jwt.decode(getToken());
      setUserIsAdmin(user.is_admin);
    })();
    return () => {};
  }, []);

  useEffect(() => {
    const getShowUser = async () => {
      const user = await showGeralId(id);
      if (user.data.senha) {
        delete user.data.senha;
      }
      setForm(user.data);
    };

    if (id) {
      setisEdit(true);
      getShowUser();
    }
  }, [id]);

  const handleChange = (event) => {
    // const checked = event.target.value === 'on' ? true : false

    const value =
      event.target.type === "checkbox"
        ? event.target.checked
        : event.target.value;
    const name = event.target.name;

    setForm({
      ...form,
      [name]: value,
    });
  };

  const formIsValid = () => {
    return form.mes && form.biel && form.duda && form.empregada && form.esocial && form.abrale && form.total ;
  };

  const submitForm = async (event) => {
    try {
      setIsSubmit(true);
      await methodUser(form);
      const is_admin = userIsAdmin ? form.is_admin : false;
      setForm({
        ...form,
        is_admin,
      });

      setAlert({
        type: "success",
        message: "Seu formulário foi enviado com sucesso",
        show: true,
      });
      setIsSubmit(false);

      setTimeout(() => history.push("/"), 3000);
    } catch (e) {
      setAlert({
        type: "error",
        message: "Ocorreu um erro no cadastro",
        show: true,
      });
      setIsSubmit(false);
    }
  };

  return (
    <React.Fragment>
      <Nav name="Lista" to="/" />
      <section>
        <Alert
          type={alert.type || ""}
          message={alert.message || ""}
          show={alert.show || false}
        />

        <div className="create_user">
          <div className="form_login">

            <FormGroup>
              <Label for="auth_mes">mes</Label>
              <Input
                disabled={isSubmit}
                type="text"
                id="auth_mes"
                name="mes"
                onChange={handleChange}
                value={form.mes || ""}
                placeholder="Qual é o mês?"
              />
            </FormGroup>

            <FormGroup>
              <Label for="auth_biel">biel</Label>
              <Input
                disabled={isSubmit}
                type="text"
                id="auth_biel"
                name="biel"
                onChange={handleChange}
                value={form.biel || ""}
                placeholder="Qual é o valor do biel?"
              />
            </FormGroup>

            <FormGroup>
              <Label for="auth_duda">duda</Label>
              <Input
                disabled={isSubmit}
                type="text"
                id="auth_duda"
                name="duda"
                onChange={handleChange}
                value={form.duda || ""}
                placeholder="Qual é o valor da duda?"
              />
            </FormGroup>
            <FormGroup>
              <Label for="auth_empregada">empregada</Label>
              <Input
                disabled={isSubmit}
                type="text"
                id="auth_empregada"
                name="empregada"
                onChange={handleChange}
                value={form.empregada || ""}
                placeholder="Qual é o valor da empregada?"
              />
            </FormGroup>
            <FormGroup>
              <Label for="auth_esocial">esocial</Label>
              <Input
                disabled={isSubmit}
                type= "text"
                id="auth_esocial"
                name="esocial"
                onChange={handleChange}
                value = {form.esocial}
                placeholder="Qual é o valor do esocial?"
              />
              
            </FormGroup>
            <FormGroup>
              <Label for="auth_abrale">abrale</Label>
              <Input
                disabled={isSubmit}
                type= "text"
                id="auth_abrale"
                name="abrale"
                onChange={handleChange}
                value = {form.abrale}
                placeholder="Qual é o valor da abrale?"
              />
              
            </FormGroup>
            <FormGroup>
              <Label for="auth_total">total</Label>
              <Input
                disabled={isSubmit}
                type= "text"
                id="auth_total"
                name="total"
                onChange={handleChange}
                value = {form.total}
                placeholder="Qual é o valor total?"
              />
              
            </FormGroup>


            <Button
              color="primary"
              disabled={!formIsValid()}
              onClick={submitForm}
            >
              {isEdit ? "Atualizar" : "Cadastrar"}
            </Button>
          </div>
          <br />
          {/* <Loading show={isSubmit}/> */}
          {isSubmit ? <div>Carregando....</div> : ""}
        </div>

      </section>
    </React.Fragment>
  );
};

export default GeralCreate;